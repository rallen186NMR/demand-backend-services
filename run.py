import storeServiceRequest
import http.server
import requests
from http.server import BaseHTTPRequestHandler
import json


class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_POST(self):
        # This is how you can read the body, its a couple steps so maybe make a helper function.
        length = int(self.headers['content-length'])
        body = self.rfile.read(length)
        # print('Body:', body)
        # BONUS: How to convert the body from a JSON string representation of a map to a python dictionary
        dictionary = json.loads(body)
        databaseresult = "No response"

        sType = dictionary.get("serviceType")
        dAddress = dictionary.get("destinationAddress")
        uName = dictionary.get("username")

        serviceRequest = storeServiceRequest.createServiceRequest(sType, dAddress, uName)

        serviceRequestID = str(serviceRequest.uuid)
        destination = str(dAddress)

        dict_to_send = {
            "serviceRequestID": serviceRequestID,
            "destination": destination
        }

        API_Call = 'https://team12.supply.softwareengineeringii.com/supply/dispatchAPI'
        response = requests.post(API_Call, json=dict_to_send) # works
        print(response.text)

        databaseresult = storeServiceRequest.insertOrderIntoDB(serviceRequest)
        self.send_response(200)

        # Very important or your response will be SLOW
        self.end_headers()
        # s = databaseresult

        # Finally, the body! The body receives bytes, so if you have a string, this is how to convert it to a bytes!
        # s = 'Success!!!!!'
        bytes = response.text.encode('utf-8')
        self.wfile.write(bytes)


httpd = http.server.HTTPServer(('', 1201), SimpleHTTPRequestHandler)
httpd.serve_forever()
