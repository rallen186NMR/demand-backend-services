import uuid
class ServiceRequest(object):
	def __init__(self, serviceType, destination, username):
		self.uuid = str(uuid.uuid4())
		self.serviceType = serviceType
		self.destinationAddress = destination
		self.user = username
