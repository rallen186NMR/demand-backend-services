# Demand API - README

The demand API receives a JSON object which at the moment receives a Service Type, an Address, and a username. Works with a supplementary Supply API and will be integrated with TM4's actual API whence formatted to work with one another.

  - Service type serves to make a a service record in the database.
  - Address will be sent to the dispatch API which will in turn send it to the mapping API, Google Maps receives an address for its functionality.
  - Username is for logging purposes.

# Functionality

  - Send JSON object from HTML to Python API
  - Communicate between Demand and Supply APIs

This text you see here is *actually* written in Markdown! To get a feel for Markdown's syntax, type some text into the left window and watch the results in the right.

### Functions

##### API CONNECTION


```sh
API_Call = 'https://team12.supply.softwareengineeringii.com/supply/dispatchAPI'
response = requests.post(API_Call, json=dict_to_send)
```

#### Deal With Response

```sh
databaseresult = storeServiceRequest.insertOrderIntoDB(serviceRequest)
```

#### Database record keeping

```sh
def insertOrderIntoDB(ServiceRequest):
    db = pymysql.connect(host="localhost", user="team12", passwd="pandapanda", db="team12_wego")
    cursor = db.cursor()

    sql = ("""INSERT INTO serviceRecord(uuid, serviceType, destinationAddress, username, vehicleID, vehicleETA, vehicleColor, vehicleLicensePlate, vehicleMake) VALUES (%s, %s, %s, %s, NULL, NULL, NULL, NULL, NULL)""")
    uuid = str(ServiceRequest.uuid)
    insert_tuple = (uuid, ServiceRequest.serviceType, ServiceRequest.destinationAddress, ServiceRequest.user)

    cursor.execute(sql, insert_tuple)
    db.commit()

    return "Successfully added order"

    cursor.close()
    db.close()
```



#### Imports

Demand API uses the following imports to accomplish its funcionality.

| Import | Purpose |
| ------ | ------ |
| http.server | Allows for HTTP handling |
| requests | Allows the file to send requests to another file |
| BaseHTTPRequestHandler | Handles requests from the browser |
| json | Allows for manipulation of JSON objects |


### Future Development

Integration with the Mapping API in order to accurately and programmatically find routes and real-time ETAs

